const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
exports.notifyUserOfNewUpdate = functions.firestore
  .document('users/{userId}')
  .onUpdate((change, context) => {
    // Get an object representing the document
    // e.g. {'name': 'Marie', 'age': 66}
    const newValue = change.after.data();
    const previousValue = change.before.data();

    // access a particular field as you would any JS property
    if (Object.keys(newValue.alerts).length > Object.keys(previousValue.alerts)
      .length) {
      console.log("NEW ALERT FOR AGENT");
      const payload = {
        notification: {
          title: 'New Alert',
          body: "New Alert ID " + Object.keys(newValue.alerts)[Object.keys(
            newValue.alerts).length - 1],
          sound: "default"
        },
        data: {
          title: "New Alert!!!!!",
          message: "NEW ALERT!!"
        }
      };
      const options = {
        priority: "high",
        timeToLive: 1 //24 hours
      };
      return admin.messaging().sendToDevice(newValue.instanceId, payload,
        options);
    } else {
      console.log("REMOVAL OF ALERT FOR AGENT");
    }

  });
