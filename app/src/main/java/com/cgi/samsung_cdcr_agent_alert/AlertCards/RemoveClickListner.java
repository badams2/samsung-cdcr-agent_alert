package com.cgi.samsung_cdcr_agent_alert.AlertCards;

/**
 * Created by b.adams on 3/26/2018.
 */

public interface RemoveClickListner {
    void OnRemoveClick(int index);
}