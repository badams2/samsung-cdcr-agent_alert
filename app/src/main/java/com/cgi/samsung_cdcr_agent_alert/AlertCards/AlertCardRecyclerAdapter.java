package com.cgi.samsung_cdcr_agent_alert.AlertCards;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cgi.samsung_cdcr_agent_alert.AlertDetailsActivity.AlertCardDetailsActivity;
import com.cgi.samsung_cdcr_agent_alert.Core.Models.Agent;
import com.cgi.samsung_cdcr_agent_alert.Core.Models.AlertData;
import com.cgi.samsung_cdcr_agent_alert.Core.MainUser;
import com.cgi.samsung_cdcr_agent_alert.Core.Utils.ListUtils;
import com.cgi.samsung_cdcr_agent_alert.R;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.WriteBatch;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by b.adams on 3/26/2018.
 */

public class AlertCardRecyclerAdapter extends RecyclerView.Adapter<AlertCardRecyclerAdapter.RecyclerItemViewHolder> {
    private ArrayList<AlertData> myList;
    int mLastPosition = 0;
    public int hiddenCards = 0;
    MainUser mnUser = MainUser.getInstance();

    public AlertCardRecyclerAdapter(ArrayList<AlertData> myList) {
        this.myList = myList;
    }

    public int getColorOfAlertText(AlertData alert) {
        if (alert.alertLevel == 3)
            return 0xFFD0021B;
        if (alert.alertLevel == 2)
            return 0xFFF5A623;
        if (alert.alertLevel == 1)
            return 0xFF17B978;
        //It should never return -1 but just in case.....
        return -1;
    }

    public int getColorOfAlertIcon(AlertData alert) {
        if (alert.alertLevel == 3)
            return R.drawable.textfill_alert_level_3;
        if (alert.alertLevel == 2)
            return R.drawable.textfill_alert_level_2;
        if (alert.alertLevel == 1)
            return R.drawable.textfill_alert_level_1;

        //It should never return -1 but just in case.....
        return -1;
    }

    public int getDrawableOfAlertText(AlertData alert) {
        if (alert.alertLevel == 3)
            return R.drawable.red_incident_icon;
        if (alert.alertLevel == 2)
            return R.drawable.yellow_incident_icon;
        if (alert.alertLevel == 1)
            return R.drawable.green_incident_icon;
        //It should never return -1 but just in case.....
        return -1;
    }

    public RecyclerItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.alert_card_main_agent_alert, parent, false);
        RecyclerItemViewHolder holder = new RecyclerItemViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerItemViewHolder holder, final int position) {
        //Log.d("onBindViewHoler ", myList.size() + "");
        AlertData alert = myList.get(position);
        Agent a = mnUser.myAgentData.get();
        boolean b = a.isEnroute.containsKey(Long.toString(alert.getAlertId()));
        holder.alertLevelTextView.setText("LEVEL " + alert.alertLevel);
        holder.alertLevelTextView.setTextColor(getColorOfAlertText(alert));
        holder.alertLevelTextView.setCompoundDrawablesWithIntrinsicBounds(getDrawableOfAlertText(alert), 0, 0, 0);
        holder.alertIDIcon.setText(Long.toString(alert.getAlertId()));
        holder.alertIDIcon.setBackgroundResource(getColorOfAlertIcon(alert));
        holder.alertDetailsTextView.setText(alert.getName());
        holder.locationDetailsTextView.setText(alert.getFriendlyLocation());
        holder.enrouteButton.setActivated(b);
        holder.distanceDetailsTextView.setText(myList.get(position).getAlertDistance());
        holder.messagesUnseenAmountTextView.setText(ListUtils.unseenMessages(position,myList));
        Format df = new SimpleDateFormat("MMM dd, hh:mm a");
        holder.alertTime.setText(df.format(alert.getCreated()));
        mLastPosition = position;
    }

    @Override
    public int getItemCount() {
        return (null != myList ? myList.size() : 0);
    }

    public void notifyData(ArrayList<AlertData> myList) {
        //Log.d("notifyData ", myList.size() + "");
        this.myList = myList;
        notifyDataSetChanged();
    }

    public class RecyclerItemViewHolder extends RecyclerView.ViewHolder {
        private final TextView alertLevelTextView;
        private final TextView alertDetailsTextView;
        private final TextView locationDetailsTextView;
        private final ImageButton messagesButton;
        private final TextView messagesUnseenAmountTextView;
        private final TextView alertIDIcon;
        private final TextView distanceDetailsTextView;
        private final ImageButton enrouteButton;
        private final TextView alertTime;
        private final ImageButton directionsButton;
        private LinearLayout cardLayout;

        public void setEnrouteinDB(int position, final boolean isEnroute) {
            AlertData alert = myList.get(position);
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            WriteBatch batch = db.batch();
            if(isEnroute == false) {
                batch.update(mnUser.myAgentData.get().agentDocRef, "isEnroute." + alert.getAlertId(), true);
                batch.commit();

            }
            else
            {
                Map<String,Boolean> newMap = mnUser.myAgentData.get().isEnroute;
                newMap.remove(Long.toString(alert.getAlertId()));
                batch.update(mnUser.myAgentData.get().agentDocRef, "isEnroute",newMap);
                batch.commit();
            }
        }

        public RecyclerItemViewHolder(final View parent) {
            super(parent);
            alertLevelTextView = (TextView) parent.findViewById(R.id.alertLevelHeader);
            alertIDIcon = (TextView) parent.findViewById(R.id.alertIDText);
            alertDetailsTextView = (TextView) parent.findViewById(R.id.alertDetailsCardContent);
            locationDetailsTextView = (TextView) parent.findViewById(R.id.locationDetailsCardContent);
            alertTime = (TextView) parent.findViewById(R.id.alertTime);
            messagesButton = (ImageButton) parent.findViewById(R.id.messagesButton);
            messagesUnseenAmountTextView = (TextView) parent.findViewById(R.id.messagesNotSeenText);
            enrouteButton = (ImageButton) parent.findViewById(R.id.enrouteButton);
            directionsButton = (ImageButton) parent.findViewById(R.id.directionsButton);
            distanceDetailsTextView = (TextView) parent.findViewById(R.id.distanceDetailsCardContent);
            cardLayout = (LinearLayout) parent.findViewById(R.id.card_details_layout);
            cardLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Open FragmentActivity with this data
                    Intent intent = new Intent(itemView.getContext(), AlertCardDetailsActivity.class);
                    intent.putExtra("cardListID", Long.toString(myList.get(getPosition()).getAlertId()));
                    intent.putExtra("alertLevel", Integer.toString(myList.get(getPosition()).getAlertLevel()));
                    itemView.getContext().startActivity(intent);
                }
            });
            messagesButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(itemView.getContext(), AlertCardDetailsActivity.class);
                    intent.putExtra("cardListID", Long.toString(myList.get(getPosition()).getAlertId()));
                    intent.putExtra("alertLevel", Integer.toString(myList.get(getPosition()).getAlertLevel()));
                    intent.putExtra("firstTab", 3);
                    itemView.getContext().startActivity(intent);
                }
            });
            enrouteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Agent a = mnUser.myAgentData.get();
                    AlertData alert = myList.get(getPosition());
                    boolean isEnroute = a.isEnroute.containsKey(Long.toString(alert.getAlertId()));
                    enrouteButton.setActivated(!isEnroute);
                    setEnrouteinDB(getPosition(),isEnroute);

                }
            });
            directionsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertData alert = myList.get(getPosition());
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + alert.getFriendlyLocation());
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    itemView.getContext().startActivity(mapIntent);
                }
            });


        }
    }
}