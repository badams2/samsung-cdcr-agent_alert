package com.cgi.samsung_cdcr_agent_alert.Core;

import android.databinding.ObservableArrayList;
import android.databinding.ObservableField;
import android.location.Location;

import com.cgi.samsung_cdcr_agent_alert.Core.Models.Agent;
import com.cgi.samsung_cdcr_agent_alert.Core.Models.AlertData;

import java.util.ArrayList;
import java.util.List;

public class MainUser {
    private static final MainUser ourInstance = new MainUser();
    public String firestoreDBID = "TERE002"; //Get it from something
    public ObservableField<Agent> myAgentData = new ObservableField<>();
    public ArrayList<AlertData> myAlertList =  new ArrayList<>();
    public static MainUser getInstance() {
        return ourInstance;
    }
    private MainUser() { }

}
