package com.cgi.samsung_cdcr_agent_alert.Core.Models;

import android.databinding.Bindable;
import android.databinding.Observable;
import android.databinding.PropertyChangeRegistry;

import com.cgi.samsung_cdcr_agent_alert.BR;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.ServerTimestamp;

import org.apache.commons.lang3.builder.DiffBuilder;
import org.apache.commons.lang3.builder.DiffResult;
import org.apache.commons.lang3.builder.Diffable;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Timer;

/**
 * Created by b.adams on 3/26/2018.
 */

public class AlertData implements Observable, Serializable {

  public Integer alertLevel;
  public boolean active;
  public String additionalInfo;
  public String alertCategory;
  public Integer radius;
  public String friendlyLocation;
  public String name;
  public GeoPoint location;
  private long alertId;
  private @ServerTimestamp Date created;
  //These items are just for me and set by me, AKA they are not from the FBFS Database
  @Exclude public String alertDistance = "N/A";
  @Exclude public double alertProximity;
  @Exclude public DocumentReference alertDocRef;
  @Exclude public ArrayList<Message> messagesList = new ArrayList<>();
  @Exclude private PropertyChangeRegistry registry = new PropertyChangeRegistry();
  @Exclude private Timer locationTimer = new Timer();

  @Override
  public String toString() {
    return "AlertData{" +
            "alertLevel=" + alertLevel +
            ", active=" + active +
            ", additionalInfo='" + additionalInfo + '\'' +
            ", alertCategory='" + alertCategory + '\'' +
            ", radius=" + radius +
            ", friendlyLocation='" + friendlyLocation + '\'' +
            ", name='" + name + '\'' +
            ", location=" + location +
            ", alertId=" + alertId +
            ", created=" + created +
            '}';
  }
  @Bindable
  public int getAlertLevel() {
    return alertLevel;
  }

  public void setAlertLevel(Integer alertLevel) {
    this.alertLevel = alertLevel;
    registry.notifyChange(this, BR.alertLevel);
  }

  @Bindable
  public ArrayList<Message> getMessagesList() {
    return messagesList;
  }

  public void setMessagesList(ArrayList<Message> m) {
    this.messagesList = m;
    registry.notifyChange(this, BR.messagesList);
  }

  @Bindable
  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
    registry.notifyChange(this, BR.active);
  }

  @Bindable
  public double getAlertProximity() {
    return alertProximity;
  }

  public void setAlertProximity(double alertProximity) {
    this.alertProximity = alertProximity;
    registry.notifyChange(this, BR.alertProximity);
  }

  @Bindable
  public String getFriendlyLocation() {
    return friendlyLocation;
  }

  public void setFriendlyLocation(String friendlyLocation) {
    this.friendlyLocation = friendlyLocation;
    registry.notifyChange(this, BR.friendlyLocation);
  }

  @Bindable
  public GeoPoint getLocation() {
    return location;
  }

  public void setLocation(GeoPoint location) {
    this.location = location;
    registry.notifyChange(this, BR.location);
  }


  @Bindable
  public long getAlertId() {
    return alertId;
  }

  public void setAlertId(long alertId) {
    this.alertId = alertId;
    registry.notifyChange(this, BR.alertId);
  }

  @Bindable
  public DocumentReference getAlertDocRef() {
    return alertDocRef;
  }

  public void setAlertDocRef(DocumentReference alertDocRef) {
    this.alertDocRef = alertDocRef;
    //registry.notifyChange(this, BR.);
  }

  @Bindable
  public String getAdditionalInfo() {
    return additionalInfo;
  }

  public void setAdditionalInfo(String additionalInfo) {
    this.additionalInfo = additionalInfo;
    registry.notifyChange(this, BR.additionalInfo);
  }

  @Bindable
  public String getAlertCategory() {
    return alertCategory;
  }

  public void setAlertCategory(String alertCategory) {
    this.alertCategory = alertCategory;
    registry.notifyChange(this, BR.alertCategory);
  }

  @Bindable
  public int getRadius() {
    return radius;
  }

  public void setRadius(Integer radius) {
    this.radius = radius;
    registry.notifyChange(this, BR.radius);
  }

  @Bindable
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
    registry.notifyChange(this, BR.name);
  }

  @Bindable
  public String getAlertDistance() {
    return alertDistance;
  }

  public void setAlertDistance(String alertDistance) {
    this.alertDistance = alertDistance;
    registry.notifyChange(this, BR.alertDistance);
  }

  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  public Timer getLocationTimer() {
    return locationTimer;
  }

  public void setLocationTimer(Timer locationTimer) {
    this.locationTimer = locationTimer;
  }

  @Override
  public void addOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
    registry.add(callback);
  }

  @Override
  public void removeOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
    registry.remove(callback);
  }

  public PropertyChangeRegistry getRegistry() {
    return registry;
  }

  public void setRegistry(PropertyChangeRegistry registry) {
    this.registry = registry;
  }

}