package com.cgi.samsung_cdcr_agent_alert.Core.Models;

import android.databinding.Bindable;
import android.databinding.Observable;
import android.databinding.PropertyChangeRegistry;

import com.cgi.samsung_cdcr_agent_alert.BR;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Agent implements Observable {
  public String agency;
  public String agentId;
  public String avatar = "";
  public String district;
  public String firstName;
  public String lastName;
  public HashMap<String,Double>  altitude;
  public HashMap<String,Double>  speed;
  public HashMap<String,Double> bearing;
  public GeoPoint location;
  @ServerTimestamp Date lastOnline;
  public String office;
  public String instanceId;
  public String phone;
  public String region;
  public String status;
  public String title;
  public String unit;
  public HashMap<String, Integer> alerts = new HashMap<>();
  public HashMap<String, Boolean> isTyping = new HashMap<>();
  public HashMap<String, Boolean> isEnroute = new HashMap<>();
  public HashMap<String, Boolean> isOnscene = new HashMap<>();
  public HashMap<String, Boolean> notResponded = new HashMap<>();
  @Exclude private PropertyChangeRegistry registry = new PropertyChangeRegistry();
  @Exclude public DocumentReference agentDocRef;


  @Override
  public String toString() {
    return "Agent{" +
            "agentID='" + agentId + '\'' +
            ", avatar='" + avatar + '\'' +
            ", district='" + district + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", agency='" + agency + '\'' +
            ", altitude='" + altitude + '\'' +
            ", speed='" + speed + '\'' +
            ", bearing='" + bearing + '\'' +
            ", location=" + location +
            ", lastOnline=" + lastOnline +
            ", office='" + office + '\'' +
            ", instanceID='" + instanceId + '\'' +
            ", phone='" + phone + '\'' +
            ", region='" + region + '\'' +
            ", status='" + status + '\'' +
            ", title='" + title + '\'' +
            ", unit='" + unit + '\'' +
            '}';
  }

  @Bindable
  public String getAgentId() {
    return agentId;
  }

  public void setAgentId(String agentId) {
    this.agentId = agentId;
    registry.notifyChange(this, BR.agentId);
  }

  @Bindable
  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
    registry.notifyChange(this, BR.avatar);
  }

  public String getDistrict() {
    return district;
  }

  public void setDistrict(String district) {
    this.district = district;
  }

  @Bindable
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getAgency() {
    return agency;
  }

  public void setAgency(String agency) {
    this.agency = agency;
  }

  public HashMap<String, Double> getAltitude() {
    return altitude;
  }

  public void setAltitude(HashMap<String, Double> altitude) {
    this.altitude = altitude;
  }

  public HashMap<String, Double> getSpeed() {
    return speed;
  }

  public void setSpeed(HashMap<String, Double> speed) {
    this.speed = speed;
  }

  public HashMap<String, Double> getBearing() {
    return bearing;
  }

  public void setBearing(HashMap<String, Double> bearing) {
    this.bearing = bearing;
  }

  @Bindable
  public GeoPoint getLocation() {
    return location;
  }

  public void setLocation(GeoPoint location) {
    this.location = location;
  }

  public Date getLastOnline() {
    return lastOnline;
  }

  public void setLastOnline(Date lastOnline) {
    this.lastOnline = lastOnline;
  }

  public String getOffice() {
    return office;
  }

  public void setOffice(String office) {
    this.office = office;
  }

  public String getInstanceId() {
    return instanceId;
  }

  public void setInstanceId(String instanceId) {
    this.instanceId = instanceId;
  }

  @Bindable
  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getRegion() {
    return region;
  }

  public void setRegion(String region) {
    this.region = region;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  @Bindable
  public String getTitle() {
    return title;
  }


  public void setTitle(String title) {
    this.title = title;
  }

  @Bindable
  public String getUnit() {
    return unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

  @Bindable
  public HashMap<String, Integer> getAlerts() {
    return alerts;
  }

  public void setAlerts(HashMap<String, Integer> alerts) {
    this.alerts = alerts;
  }

  public HashMap<String, Boolean> getIsTyping() {
    return isTyping;
  }

  public void setIsTyping(HashMap<String, Boolean> isTyping) {
    this.isTyping = isTyping;
  }

  public HashMap<String, Boolean> getIsEnroute() {
    return isEnroute;
  }

  public void setIsEnroute(HashMap<String, Boolean> isEnroute) {
    this.isEnroute = isEnroute;
  }

  public HashMap<String, Boolean> getIsOnscene() {
    return isOnscene;
  }

  public void setIsOnscene(HashMap<String, Boolean> isOnscene) {
    this.isOnscene = isOnscene;
  }

  public HashMap<String, Boolean> getNotResponded() {
    return notResponded;
  }

  public void setNotResponded(HashMap<String, Boolean> notResponded) {
    this.notResponded = notResponded;
  }

  public DocumentReference getAgentDocRef() {
    return agentDocRef;
  }

  public void setAgentDocRef(DocumentReference agentDocRef) {
    this.agentDocRef = agentDocRef;
  }

  @Override
  public void addOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
    registry.add(callback);
  }

  @Override
  public void removeOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
    registry.remove(callback);
  }

}