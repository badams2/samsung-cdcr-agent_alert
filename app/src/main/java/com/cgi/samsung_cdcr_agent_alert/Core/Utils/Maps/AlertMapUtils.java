package com.cgi.samsung_cdcr_agent_alert.Core.Utils.Maps;

import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsLeg;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.TravelMode;

import org.joda.time.DateTime;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.concurrent.TimeUnit;

public class AlertMapUtils {

    private static GeoApiContext getGeoContext() {
        GeoApiContext geoApiContext = new GeoApiContext();
        return geoApiContext
                .setQueryRateLimit(8)
                .setApiKey("AIzaSyCCEAzv9Ok3qRNluKf_dN-Tdrdx7AQ122U")
                .setConnectTimeout(30, TimeUnit.SECONDS)
                .setReadTimeout(30, TimeUnit.SECONDS)
                .setWriteTimeout(30, TimeUnit.SECONDS);
    }
    public static DirectionsResult getDirectionsDetails(String origin, String destination, TravelMode mode) {
        DateTime now = new DateTime();
        try {
            return DirectionsApi.newRequest(getGeoContext())
                    .mode(mode)
                    .origin(origin)
                    .destination(destination)
                    .departureTime(now)
                    .await();
        } catch (ApiException e) {
            e.printStackTrace();
            return null;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static String[] calculateDistanceAndDurations(DirectionsResult results)
    {
        double totalDistance = 0;
        double totalDuration = 0;
        String[] distanceAndDurationInMilesAndHours = new String[2];
        DirectionsLeg[] legs = results.routes[0].legs;
        for(int i = 0; i < legs.length; ++i)
        {
            totalDistance += legs[i].distance.inMeters;
            totalDuration += legs[i].duration.inSeconds;
        }
        DecimalFormat df = new DecimalFormat("#.0");
        distanceAndDurationInMilesAndHours[0] = df.format(totalDistance/1609.344);
        distanceAndDurationInMilesAndHours[1] = "~" + df.format (totalDuration/60) + " Min.";
        return distanceAndDurationInMilesAndHours;

    }
}
