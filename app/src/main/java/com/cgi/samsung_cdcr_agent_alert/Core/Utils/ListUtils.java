package com.cgi.samsung_cdcr_agent_alert.Core.Utils;

import android.databinding.ObservableArrayList;

import com.cgi.samsung_cdcr_agent_alert.Core.MainUser;
import com.cgi.samsung_cdcr_agent_alert.Core.Models.AlertData;
import com.cgi.samsung_cdcr_agent_alert.Core.Models.Message;

import java.util.ArrayList;

public class ListUtils {
    public static int getIndexByAlertIDProperty(String alertID, ArrayList<AlertData> list) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) !=null && Long.toString(list.get(i).getAlertId()).equals(alertID)) {
                return i;
            }
        }
        return -1;// not there is list
    }
    public static String unseenMessages(int position, ArrayList<AlertData> myList)
    {
        MainUser mnUser = MainUser.getInstance();
        int numOfUnseenMessages = 0;
        for(Message m : myList.get(position).messagesList)
        {
            if(!m.getSeenBy().containsKey(mnUser.firestoreDBID))
                numOfUnseenMessages++;
        }
        return Integer.toString(numOfUnseenMessages);
    }
}
