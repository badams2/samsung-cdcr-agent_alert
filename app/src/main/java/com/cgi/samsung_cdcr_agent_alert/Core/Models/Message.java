package com.cgi.samsung_cdcr_agent_alert.Core.Models;

import com.cgi.samsung_cdcr_agent_alert.Core.Models.Agent;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;
import java.util.HashMap;

public class Message {
  Agent from;
  String msg;
  @ServerTimestamp Date msgSentTime;
  //this should just be a list of names or userID's
  HashMap<String,Boolean> seenBy = new HashMap<>();
  @Exclude
  DocumentReference docRef;

  @Override
  public String toString() {
    return "Message{" +
            "from='" + from + '\'' +
            ", msg='" + msg + '\'' +
            ", msgSentTime=" + msgSentTime +
            ", seenBy=" + seenBy +
            '}';
  }

  public Agent getFrom() {
    return from;
  }

  public void setFrom(Agent from) {
    this.from = from;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public HashMap<String,Boolean> getSeenBy() {
    return seenBy;
  }

  public void setSeenBy(HashMap<String,Boolean> seenBy) {
    this.seenBy = seenBy;
  }

  public Date getMsgSentTime() {
    return msgSentTime;
  }

  public void setMsgSentTime(Date msgSentTime) {
    this.msgSentTime = msgSentTime;
  }

  public DocumentReference getDocRef() {
    return docRef;
  }

  public void setDocRef(DocumentReference docRef) {
    this.docRef = docRef;
  }
}