package com.cgi.samsung_cdcr_agent_alert.Core.Utils.Firebase;

import android.location.Location;
import android.support.annotation.NonNull;
import android.util.Log;

import com.cgi.samsung_cdcr_agent_alert.Core.MainUser;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.firebase.firestore.WriteBatch;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class FirebaseLocationUpdateService {
    public static final String TAG = "Firebase Location Service";
    public static FirebaseFirestore db = FirebaseFirestore.getInstance();
    public static MainUser mnUser = MainUser.getInstance();
    @ServerTimestamp
    static Date date;

    public static void postMyLocationtoDB(Location location) {
        date = new Date();
        if(mnUser.myAgentData.get() != null) {
            mnUser.myAgentData
                    .get()
                    .getAgentDocRef()
                    .update("location", new GeoPoint(location.getLatitude(), location.getLongitude()),"lastOnline",date)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.d(TAG, "Agent Location Update Successful");
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d(TAG, "Agent Location Update Unsuccessful");
                        }
                    });
        }
    }
    public static void postMyLocationtoAlert(Location location, DocumentReference doc) {
        HashMap<String,Object> temp = new HashMap<>();
        temp.put("agentId", MainUser.getInstance().firestoreDBID);
        temp.put("location", new GeoPoint(location.getLatitude(),location.getLongitude()));
        temp.put("time", new Date());

        doc.collection("Members").add(temp).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
            Log.d("Alert Location Timestamp", "Succesful Send");
            }
        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("Alert Location", "Unsuccesful Send");
                    }
                });

    }
}
