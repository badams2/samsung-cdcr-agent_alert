package com.cgi.samsung_cdcr_agent_alert.Core.Utils.Firebase;

import android.support.annotation.NonNull;
import android.util.Log;

import com.cgi.samsung_cdcr_agent_alert.Core.MainUser;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.Transaction;
import com.google.firebase.firestore.WriteBatch;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import static com.cgi.samsung_cdcr_agent_alert.AlertLocationService.LocationService.TAG;

public class AgentAlertFirebaseInstanceIDService extends FirebaseInstanceIdService {
    MainUser mnUser = MainUser.getInstance();
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("FB TOKEN", "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }

    public void sendRegistrationToServer(String token)
    {
        WriteBatch wb = db.batch();
        db.collection("users")
                .whereEqualTo("agentId", mnUser.firestoreDBID)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                       DocumentReference docRef = queryDocumentSnapshots.getDocuments()
                               .get(0).getReference();
                       wb.update(docRef,"instanceId", token);
                       wb.commit();
                    }
                });
    }
}
