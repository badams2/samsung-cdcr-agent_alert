package com.cgi.samsung_cdcr_agent_alert.AlertMap;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.cgi.samsung_cdcr_agent_alert.Core.Models.AlertData;
import com.cgi.samsung_cdcr_agent_alert.Core.MainUser;
import com.cgi.samsung_cdcr_agent_alert.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;

import java.util.List;


public class AlertMapActivity extends FragmentActivity implements OnMapReadyCallback {

  private GoogleMap mMap;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_alert_map);
    // Obtain the SupportMapFragment and get notified when the map is ready to be used.
    SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
            .findFragmentById(R.id.map);
    FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabMapExit);
    fab.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        finish();
      }
    });
    mapFragment.getMapAsync(this);
  }

  @Override
  public void onMapReady(GoogleMap googleMap) {
    mMap = googleMap;

    List<AlertData> alerts = MainUser.getInstance().myAlertList;
    if(alerts.size() != 0) {
      LatLngBounds.Builder builder = new LatLngBounds.Builder();
      for (AlertData alert : alerts) {
        LatLng alertLatLng = new LatLng(alert.getLocation().getLatitude(),
                alert.getLocation().getLongitude());
        builder.include(alertLatLng);
        LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = vi.inflate(R.layout.alert_id_holder, null);
        TextView textView = (TextView) v.findViewById(R.id.alertIDText);
        textView.setText(Long.toString(alert.getAlertId()));
        if (alert.alertLevel == 1) {
          textView.setBackgroundResource(R.drawable.textfill_alert_level_1);
        } else if (alert.alertLevel == 2) {
          textView.setBackgroundResource(R.drawable.textfill_alert_level_2);
        } else if (alert.alertLevel == 3) {
          textView.setBackgroundResource(R.drawable.textfill_alert_level_3);
        }

        IconGenerator iconGenerator = new IconGenerator(this);
        iconGenerator.setContentView(v);
        mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon()))
                .position(alertLatLng)
                .draggable(false));
      }
      LatLngBounds bounds = builder.build();
      int padding = 100; // offset from edges of the map in pixels
      mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding));
    }
  }
}