package com.cgi.samsung_cdcr_agent_alert;

import android.Manifest;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.databinding.Observable;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cgi.samsung_cdcr_agent_alert.AlertCards.AlertCardRecyclerAdapter;
import com.cgi.samsung_cdcr_agent_alert.AlertLocationService.LocationService;
import com.cgi.samsung_cdcr_agent_alert.AlertMap.AlertMapActivity;
import com.cgi.samsung_cdcr_agent_alert.Core.MainUser;
import com.cgi.samsung_cdcr_agent_alert.Core.Models.Agent;
import com.cgi.samsung_cdcr_agent_alert.Core.Models.AlertData;
import com.cgi.samsung_cdcr_agent_alert.Core.Models.Message;
import com.cgi.samsung_cdcr_agent_alert.Core.Utils.Maps.AlertMapUtils;
import com.crashlytics.android.Crashlytics;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.TravelMode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.TimerTask;

import javax.annotation.Nullable;

import de.hdodenhof.circleimageview.CircleImageView;
import io.fabric.sdk.android.Fabric;

import static com.cgi.samsung_cdcr_agent_alert.Core.Utils.NetworkUtil.TYPE_MOBILE;
import static com.cgi.samsung_cdcr_agent_alert.Core.Utils.NetworkUtil.TYPE_NOT_CONNECTED;
import static com.cgi.samsung_cdcr_agent_alert.Core.Utils.NetworkUtil.TYPE_WIFI;

public class MainAgentAlertActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    public AlertCardRecyclerAdapter mRecyclerAdapter;
    public ObservableList<AlertData> test = new ObservableArrayList<>();
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    MainUser mnUser;
    RadioButton all_alerts;
    private FirebaseAuth mAuth;
    RadioButton level_1;
    RadioButton level_2;
    RadioButton level_3;
    String alertLevel = "", alertDescription = "", alertLocation = "", alertDistance = "", alertFirebaseID = "";
    boolean desktopModeEnabled = false;
    boolean currentDesktopMode = false;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView emptyView;
    private RelativeLayout relativeLayout;
    private boolean internetConnected = true;
    private Toolbar toolbar;
    private ArrayList<AlertData> myAlertList = new ArrayList<>();
    private static final int RC_SIGN_IN = 123;
    private Snackbar snackbar;
    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String status = getConnectivityStatusString(context);
            setSnackbarMessage(status, false);
        }
    };
    public void createSignInIntent() {
        // [START auth_fui_create_intent]
        // Choose authentication providers
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.PhoneBuilder().build());

        // Create and launch sign-in intent
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .build(),
                RC_SIGN_IN);
        // [END auth_fui_create_intent]
    }

    // [START auth_fui_result]
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                // Successfully signed in
                //FirebaseUser currentUser = mAuth.getCurrentUser();
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                String phoneNumber = String.valueOf(user.getPhoneNumber()).replace("+1","").replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1-$2-$3");
                Log.e("AUTH",phoneNumber);
                checkPermissions();
                getAlertData(phoneNumber);
                init();
                // ...
            } else {
                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                // ...
            }
        }
    }
    public void checkCurrentUser() {
        // [START check_current_user]
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            // User is signed in
            String phoneNumber = String.valueOf(user.getPhoneNumber()).replace("+1","").replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1-$2-$3");
            Log.e("AUTH",phoneNumber);

            getAlertData(phoneNumber);
            init();
        } else {
            //Try to sign them in
            createSignInIntent();
        }
        // [END check_current_user]
    }

    public static int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public static String getConnectivityStatusString(Context context) {
        int conn = getConnectivityStatus(context);
        String status = null;
        if (conn == TYPE_WIFI) {
            status = "Wifi enabled";
        } else if (conn == TYPE_MOBILE) {
            status = "Mobile data enabled";
        } else if (conn == TYPE_NOT_CONNECTED) {
            status = "Not connected to Internet";
        }
        return status;
    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
                || !manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void checkPermissions() {
        final String[] requiredPermissions = {
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.CALL_PHONE
        };
        final List<String> neededPermissions = new ArrayList<>();
        for (final String permission : requiredPermissions) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    permission) != PackageManager.PERMISSION_GRANTED) {
                neededPermissions.add(permission);
            }
        }
        if (!neededPermissions.isEmpty()) {
            requestPermissions(neededPermissions.toArray(new String[]{}), 1);
        } else {
            //statusCheck();
            Intent locationService = new Intent(getApplicationContext(), LocationService.class);
            startService(locationService);
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent locationService = new Intent(getApplicationContext(), LocationService.class);
                    startService(locationService);
                } else {
                    Log.d("Permission Manager: ", "Permission Denied for Location");
                }
                return;
            }

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("Agent Alert" ,"onCreate");



    }

    public void setVisibilityOfEmptyView() {
        if (this.myAlertList.isEmpty()) {
            mRecyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        }
    }

    //This is gross, this is REALLLLLY gross but better than relying on basically an untestable god service.
    public void getAlertData(String phoneNumber) {
        db.collection("users").whereEqualTo("phone", phoneNumber)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if(!queryDocumentSnapshots.isEmpty())
                        {
                        Agent mainAgent = queryDocumentSnapshots.getDocuments().get(0).toObject(Agent.class);
                        mainAgent.setAgentDocRef(queryDocumentSnapshots.getDocuments().get(0).getReference());
                        mnUser.myAgentData.set(mainAgent);
                        for (String key : mainAgent.getAlerts().keySet()) {
                            if (myAlertList.stream().noneMatch(a -> Long.toString(a.getAlertId()).equals(key))) {
                                db.collection("alerts").document(key).addSnapshotListener(new EventListener<DocumentSnapshot>() {
                                    @Override
                                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                                        AlertData alert = documentSnapshot.toObject(AlertData.class);
                                        alert.setAlertDocRef(documentSnapshot.getReference());
                                        long alertLocationPingTimer = 0;
                                        switch (alert.getAlertLevel()) {
                                            case 1:
                                                alertLocationPingTimer = 60000L;
                                                break;
                                            case 2:
                                                alertLocationPingTimer = 300000L;
                                                break;
                                            case 3:
                                                alertLocationPingTimer = 900000L;
                                                break;
                                            default:
                                                alertLocationPingTimer = -1L;
                                                break;

                                        }
                                        Log.d("timer", Long.toString(alertLocationPingTimer));
                                        alert.getLocationTimer().scheduleAtFixedRate(new TimerTask() {
                                            @Override
                                            public void run() {
                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        if (mnUser.myAgentData.get().location != null) {
                                                            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                                                            Location loc = new Location("system");
                                                            loc.setLatitude(mnUser.myAgentData.get().location.getLatitude());
                                                            loc.setLongitude(mnUser.myAgentData.get().location.getLongitude());
                                                            List<Address> addressesOrigin = new ArrayList<Address>();
                                                            List<Address> addressesDestination = new ArrayList<Address>();
                                                            try {
                                                                addressesOrigin = geocoder.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
                                                            } catch (IOException e) {
                                                            }
                                                            // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                                                            String addressOrigin = addressesOrigin.get(0).getAddressLine(0);
                                                            DirectionsResult results = AlertMapUtils.getDirectionsDetails(addressOrigin, alert.getFriendlyLocation(), TravelMode.DRIVING);
                                                            if (results != null) {
                                                                String[] temp = AlertMapUtils.calculateDistanceAndDurations(results);
                                                                alert.setAlertDistance(temp[0] + " Miles" + "  " + temp[1]);
                                                                alert.setAlertProximity(Double.valueOf(temp[0]));
                                                                mRecyclerAdapter.notifyData(myAlertList);
                                                            }
                                                            //FirebaseLocationUpdateService.postMyLocationtoAlert(loc , alert.getAlertDocRef());
                                                        }
                                                    }
                                                });
                                            }
                                        }, 10, alertLocationPingTimer);

                                        alert.getAlertDocRef().collection("Messages")
                                                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                                    @Override
                                                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                                                        alert.setMessagesList((ArrayList) queryDocumentSnapshots.toObjects(Message.class));
                                                        mRecyclerAdapter.notifyData(myAlertList);

                                                    }
                                                });

                                        myAlertList.add(alert);
                                        mnUser.myAlertList.add(alert);
                                        setVisibilityOfEmptyView();
                                        mRecyclerAdapter.notifyData(myAlertList);
                                    }
                                });
                            }
                        }
                    }
                }});

    }
@Override
public void onStart()
{
    super.onStart();
    Log.e("Agent Alert" ,"onStart");
    Fabric.with(this, new Crashlytics());
    mnUser = MainUser.getInstance();
    setContentView(R.layout.activity_main_agent_alert_with_nav);

    // initialize the status for Samsung DeX mode enabled
    //desktopModeEnabled = checkDeXEnabled();
    relativeLayout = (RelativeLayout) findViewById(R.id.main_activity_content);
    FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
            .setPersistenceEnabled(true)
            .build();
    db.setFirestoreSettings(settings);
    checkCurrentUser();


}

    public void init() {
        mRecyclerView = (RecyclerView) findViewById(R.id.alert_card_list);
        emptyView = (TextView) findViewById(R.id.empty_view);
        mRecyclerAdapter = new AlertCardRecyclerAdapter(myAlertList);
        setVisibilityOfEmptyView();
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mRecyclerAdapter);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setRadioButtonLogic();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent tutorialPage = new Intent(getApplicationContext(), AlertMapActivity.class);
                startActivity(tutorialPage);

            }
        });
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        if (!isMyServiceRunning(LocationService.class)) {
            Log.e("Agent Alert", "Location Service Not Running Attempting to Start");
            checkPermissions();
        }

        CircleImageView navProfileAvatar = (CircleImageView) navigationView.getHeaderView(0)
                .findViewById(R.id.profile_image_nav);
        TextView agentName = (TextView) navigationView.getHeaderView(0)
                .findViewById(R.id.agentName);
        mnUser.myAgentData.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                agentName.setText(mnUser.myAgentData.get().firstName
                        + " " + mnUser.myAgentData.get().lastName);
                Glide.with(getApplicationContext())
                        .load(mnUser.myAgentData.get().avatar)
                        .into(navProfileAvatar);
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        currentDesktopMode = checkDeXEnabled();
        if (desktopModeEnabled != currentDesktopMode) {
            desktopModeEnabled = currentDesktopMode;
            // Here, implement the working codes when switch the mode between mobile and Samsung DeX
        }
    }

    boolean checkDeXEnabled() {
        boolean enabled;
        Configuration config = getResources().getConfiguration();
        try {
            Class configClass = config.getClass();
            if (configClass.getField("SEM_DESKTOP_MODE_ENABLED").getInt(configClass)
                    == configClass.getField("semDesktopModeEnabled").getInt(config)) {
                enabled = true;
            } else {
                enabled = false;
            }
            return enabled;
        } catch (NoSuchFieldException e) {
        } catch (IllegalAccessException e) {
        } catch (IllegalArgumentException e) {
        }
        return false;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_agent_alert, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void showCards(int button) {
        ArrayList<AlertData> temp = new ArrayList<>();
        for (AlertData ad : myAlertList) {
            temp.add(ad);
        }
        Log.d("TAG", Integer.toString(temp.size()));
        switch (button) {
            case 0:
                mRecyclerAdapter.notifyData(myAlertList);
                break;
            case 1:
                temp.removeIf(a -> a.alertLevel != 1);
                mRecyclerAdapter.notifyData(temp);
                break;
            case 2:
                temp.removeIf(a -> a.alertLevel != 2);
                mRecyclerAdapter.notifyData(temp);
                break;
            case 3:
                temp.removeIf(a -> a.alertLevel != 3);
                mRecyclerAdapter.notifyData(temp);
                break;
        }
    }

    public void setRadioButtonLogic() {
        all_alerts = (RadioButton) findViewById(R.id.alert_all_filter_radio_button);
        level_1 = (RadioButton) findViewById(R.id.alert_level_1_filter_radio_button);
        level_2 = (RadioButton) findViewById(R.id.alert_level_2_filter_radio_button);
        level_3 = (RadioButton) findViewById(R.id.alert_level_3_filter_radio_button);

        all_alerts.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                all_alerts.setChecked(true);
                level_1.setChecked(false);
                level_2.setChecked(false);
                level_3.setChecked(false);
                showCards(0);
            }
        });

        level_1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                level_1.setChecked(true);
                all_alerts.setChecked(false);
                level_2.setChecked(false);
                level_3.setChecked(false);
                showCards(1);
            }
        });

        level_2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                level_2.setChecked(true);
                level_1.setChecked(false);
                all_alerts.setChecked(false);
                level_3.setChecked(false);
                showCards(2);

            }
        });
        level_3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                level_3.setChecked(true);
                level_1.setChecked(false);
                level_2.setChecked(false);
                all_alerts.setChecked(false);
                showCards(3);

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_filter) {
            LinearLayout radioButtonLayout = (LinearLayout) findViewById(R.id.radio_button_layout);
            if (radioButtonLayout.getVisibility() == View.GONE) {
                radioButtonLayout.setVisibility(View.VISIBLE);
            } else {
                radioButtonLayout.setVisibility(View.GONE);
            }
            return true;
        }
        if (id == R.id.form1) {
            Collections.sort(myAlertList, new Comparator<AlertData>() {
                public int compare(AlertData o1, AlertData o2) {
                    if (o1.getCreated() == null || o2.getCreated() == null)
                        return 0;
                    return o2.getCreated().compareTo(o1.getCreated());
                }
            });
            mRecyclerAdapter.notifyData(myAlertList);
            return true;
        }
        if (id == R.id.form2) {
            Collections.sort(myAlertList, new Comparator<AlertData>() {
                public int compare(AlertData o1, AlertData o2) {
                    if (o1.getAlertDistance() == null || o2.getAlertDistance() == null)
                        return 0;
                    return Double.compare(o1.getAlertProximity(), o2.getAlertProximity());
                }
            });
            mRecyclerAdapter.notifyData(myAlertList);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void registerInternetCheckReceiver() {
        IntentFilter internetFilter = new IntentFilter();
        internetFilter.addAction("android.net.wifi.STATE_CHANGE");
        internetFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(broadcastReceiver, internetFilter);
    }

    private void setSnackbarMessage(String status, boolean showBar) {
        String internetStatus = "";
        if (status.equalsIgnoreCase("Wifi enabled") || status.equalsIgnoreCase("Mobile data enabled")) {
            internetStatus = "Internet Connected";
        } else {
            internetStatus = "Lost Internet Connection";
        }
        snackbar = Snackbar
                .make(relativeLayout, internetStatus, Snackbar.LENGTH_LONG)
                .setAction("X", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        snackbar.dismiss();
                    }
                });
        // Changing message text color
        snackbar.setActionTextColor(Color.WHITE);
        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        if (internetStatus.equalsIgnoreCase("Lost Internet Connection")) {
            if (internetConnected) {
                snackbar.setDuration(5000).show();
                internetConnected = false;
            }
        } else {
            if (!internetConnected) {
                internetConnected = true;
                snackbar.setDuration(5000).show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerInternetCheckReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }
}