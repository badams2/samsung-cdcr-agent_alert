package com.cgi.samsung_cdcr_agent_alert.AlertDetailsActivity.AlertCardDetailsFragments.AlertMessaging;

import android.icu.text.SimpleDateFormat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cgi.samsung_cdcr_agent_alert.AlertCards.RemoveClickListner;
import com.cgi.samsung_cdcr_agent_alert.Core.MainUser;
import com.cgi.samsung_cdcr_agent_alert.Core.Models.Message;
import com.cgi.samsung_cdcr_agent_alert.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import de.hdodenhof.circleimageview.CircleImageView;

public class AlertMessagingRecyclerAdapter extends RecyclerView.Adapter {
    private ArrayList<Message> myList;
    int mLastPosition = 0;
    private RemoveClickListner mListner;
    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;

    public AlertMessagingRecyclerAdapter(ArrayList<Message> myList, RemoveClickListner listner) {
        this.myList = myList;
        mListner = listner;
    }

    @Override
    public int getItemViewType(int position) {
        Message message = myList.get(position);
        if (message.getFrom().getAgentId().equals(MainUser.getInstance().firestoreDBID)) {
            // If the current user is the sender of the message
            return VIEW_TYPE_MESSAGE_SENT;
        } else {
            // If some other user sent the message
            return VIEW_TYPE_MESSAGE_RECEIVED;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.alert_single_message_self, parent, false);
            return new SentItemViewHolder(view);
        } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.alert_single_message_team, parent, false);
            return new ReceivedItemViewHolder(view);
        }
        return null;
    }

    public boolean isLastInGroup(int position) {
        if (position == myList.size() - 1) {
            return true;
        }
        if (!(myList.get(position).getFrom().getAgentId()
                .equals(myList.get(position + 1).getFrom().getAgentId()))) {
            return true;
        }
        return false;
    }
    public boolean isFirstInGroup(int position) {
        if (position == 0) {
            return true;
        }
        if (!(myList.get(position).getFrom().getAgentId()
                .equals(myList.get(position - 1).getFrom().getAgentId()))) {
            return true;
        }
        return false;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Message message = myList.get(position);
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_SENT:
                if (isLastInGroup(position)) {
                    ((SentItemViewHolder) holder)
                            .messageFromAndTimeLayout.setVisibility(View.VISIBLE);
                } else {
                    ((SentItemViewHolder) holder)
                            .messageFromAndTimeLayout.setVisibility(View.GONE);
                }
                ((SentItemViewHolder) holder).bind(message);
                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:
                if (isLastInGroup(position)) {
                    ((ReceivedItemViewHolder) holder)
                            .messageFromAndTimeLayout.setVisibility(View.VISIBLE);
                } else {
                    ((ReceivedItemViewHolder) holder)
                            .messageFromAndTimeLayout.setVisibility(View.GONE);
                }
                if (isFirstInGroup(position)) {
                    ((ReceivedItemViewHolder) holder)
                            .circleProfileView.setVisibility(View.VISIBLE);
                    ((ReceivedItemViewHolder) holder)
                            .onlineStatusView.setVisibility(View.VISIBLE);

                } else {
                    ((ReceivedItemViewHolder) holder)
                            .circleProfileView.setVisibility(View.GONE);
                    ((ReceivedItemViewHolder) holder)
                            .onlineStatusView.setVisibility(View.GONE);

                }
                ((ReceivedItemViewHolder) holder).bind(message);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return (null != myList ? myList.size() : 0);
    }

    public void notifyData(ArrayList<Message> myList) {
        //organize list
        Collections.sort(myList, new Comparator<Message>() {
            public int compare(Message o1, Message o2) {
                return o1.getMsgSentTime().compareTo(o2.getMsgSentTime());
            }
        });
        this.myList = myList;
        notifyDataSetChanged();
    }

    private class SentItemViewHolder extends RecyclerView.ViewHolder {
        private final TextView messageTextView;
        private final TextView messageTimeTextView;
        private LinearLayout messageFromAndTimeLayout;

        public SentItemViewHolder(final View parent) {
            super(parent);
            messageTextView = (TextView) parent.findViewById(R.id.messageText);
            messageTimeTextView = (TextView) parent.findViewById(R.id.messageTimeText);
            messageFromAndTimeLayout = (LinearLayout) parent.findViewById(R.id.messageFromAndTimeLayoutSelf);
        }

        void bind(Message message) {
            messageTextView.setText(message.getMsg());
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
            messageTimeTextView.setText(sdf.format(message.getMsgSentTime()));
        }
    }

    private class ReceivedItemViewHolder extends RecyclerView.ViewHolder {
        private final TextView messageTextView;
        private final TextView messageTimeTextView;
        private final TextView messageFromTextView;
        private LinearLayout messageFromAndTimeLayout;
        private CircleImageView circleProfileView;
        private ImageView  onlineStatusView;

        public ReceivedItemViewHolder(final View parent) {
            super(parent);
            messageTextView = (TextView) parent.findViewById(R.id.messageText);
            messageTimeTextView = (TextView) parent.findViewById(R.id.messageTimeText);
            messageFromAndTimeLayout = (LinearLayout) parent.findViewById(R.id.messageFromAndTimeLayoutTeam);
            messageFromTextView = (TextView) parent.findViewById(R.id.messageFromText);
            circleProfileView = (CircleImageView)parent.findViewById(R.id.profile_image);
            onlineStatusView = (ImageView) parent.findViewById(R.id.online_status);
        }

        void bind(Message message) {
            messageTextView.setText(message.getMsg());
            messageFromTextView.setText(message.getFrom().firstName + " " + message.getFrom().lastName);
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
            messageTimeTextView.setText(sdf.format(message.getMsgSentTime()));
            Glide.with(itemView.getContext()).load(message.getFrom().getAvatar()).into(circleProfileView);
        }
    }
}