package com.cgi.samsung_cdcr_agent_alert.AlertDetailsActivity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.cgi.samsung_cdcr_agent_alert.AlertDetailsActivity.AlertCardDetailsFragments.AlertDetails.AlertDetailsFragment;
import com.cgi.samsung_cdcr_agent_alert.AlertDetailsActivity.AlertCardDetailsFragments.AlertMessaging.AlertMessagingFragment;
import com.cgi.samsung_cdcr_agent_alert.AlertDetailsActivity.AlertCardDetailsFragments.AlertPeople.AlertPeopleFragment;


/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    public Bundle b;
    public SectionsPagerAdapter(FragmentManager fm, Bundle bundle) {
        super(fm); b = bundle;
    }

    @Override
    public Fragment getItem(int position) {
        switch(position) {
            case 0: return AlertDetailsFragment.newInstance(0,b);
            case 1: return AlertPeopleFragment.newInstance(1,b);
            case 2: return AlertMessagingFragment.newInstance(2,b);
            default: return AlertDetailsFragment.newInstance(position + 1,b);
        }
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 3;
    }
}
