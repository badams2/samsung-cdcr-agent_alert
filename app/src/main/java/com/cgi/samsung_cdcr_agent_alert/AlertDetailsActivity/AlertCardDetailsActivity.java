package com.cgi.samsung_cdcr_agent_alert.AlertDetailsActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cgi.samsung_cdcr_agent_alert.Core.Models.AlertData;
import com.cgi.samsung_cdcr_agent_alert.R;
import com.google.maps.android.ui.IconGenerator;

import java.util.ArrayList;
import java.util.List;

public class AlertCardDetailsActivity extends AppCompatActivity {

  private SectionsPagerAdapter mSectionsPagerAdapter;
  private ViewPager mViewPager;

  class ViewPagerAdapter extends FragmentPagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager manager) {
      super(manager);
    }

    @Override
    public Fragment getItem(int position) {
      return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
      return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
      mFragmentList.add(fragment);
      mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
      return mFragmentTitleList.get(position);
    }
  }
public IconGenerator getIcon(String alertLevel, String alertID)
{
  LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  View v = vi.inflate(R.layout.alert_id_holder, null);
  TextView textView = (TextView) v.findViewById(R.id.alertIDText);
  textView.setText(alertID);
  if (alertLevel == "1") {
    textView.setBackgroundResource(R.drawable.textfill_alert_level_1);
  } else if (alertLevel == "2") {
    textView.setBackgroundResource(R.drawable.textfill_alert_level_2);
  } else if (alertLevel == "3") {
    textView.setBackgroundResource(R.drawable.textfill_alert_level_3);
  }

  IconGenerator iconGenerator = new IconGenerator(this);
  iconGenerator.setContentView(v);
  return iconGenerator;
}
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_alert_card_details);
    Intent intent = getIntent();
    String id = intent.getStringExtra("cardListID");
    AlertData a = intent.getParcelableExtra("AlertData");
    String alertLevelString = intent.getStringExtra("alertLevel");
    Log.d("ALERT ID FROM MAIN FRAG ACTIVITY", id);
    Bundle bundle = new Bundle();
    bundle.putLong("cardListID",Long.parseLong(id));
    TextView alertLevelTextView = (TextView) findViewById(R.id.level_text);
    alertLevelTextView.setText("LEVEL " + alertLevelString);
    Log.d("COLOR", alertLevelString);
    TextView alertIDTextView = (TextView) findViewById(R.id.alertIDText);
    if (alertLevelString.equals("1")) {
      alertIDTextView.setBackgroundResource(R.drawable.textfill_alert_level_1);
    } else if (alertLevelString.equals("2")) {
      Log.d("SETTING COLOR", "SETTING");
      alertIDTextView.setBackgroundResource(R.drawable.textfill_alert_level_2);
    } else if (alertLevelString.equals("3")) {
      alertIDTextView.setBackgroundResource(R.drawable.textfill_alert_level_3);
    }
    alertIDTextView.setText(id);
    ImageView backButton = (ImageView) findViewById(R.id.back_button);
    backButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        finish();
      }
    });

    // Create the adapter that will return a fragment for each of the three
    // primary sections of the activity.
    mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),bundle);

    // Set up the ViewPager with the sections adapter.
    mViewPager = (ViewPager) findViewById(R.id.container);
    mViewPager.setAdapter(mSectionsPagerAdapter);

    TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
    mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
    tabLayout.setOnTabSelectedListener(
            new TabLayout.ViewPagerOnTabSelectedListener(mViewPager) {

              @Override
              public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                //int tabIconColor = ContextCompat.getColor(getApplicationContext(),R.color.colorWhite);
                //tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
              }

              @Override
              public void onTabUnselected(TabLayout.Tab tab) {
                super.onTabUnselected(tab);
                //int tabIconColor = ContextCompat.getColor(getApplicationContext(), R.color.cardview_dark_background);
                //tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
              }

              @Override
              public void onTabReselected(TabLayout.Tab tab) {
                super.onTabReselected(tab);
              }
            }
    );
    int ifFirstTab = intent.getIntExtra("firstTab",0);
    if(ifFirstTab != 0) {
      mViewPager.setCurrentItem(ifFirstTab);
    }
  }
}