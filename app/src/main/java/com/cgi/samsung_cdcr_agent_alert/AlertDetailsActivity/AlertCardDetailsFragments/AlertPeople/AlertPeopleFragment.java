package com.cgi.samsung_cdcr_agent_alert.AlertDetailsActivity.AlertCardDetailsFragments.AlertPeople;

import android.databinding.ObservableArrayList;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cgi.samsung_cdcr_agent_alert.Core.Models.Agent;
import com.cgi.samsung_cdcr_agent_alert.Core.MainUser;
import com.cgi.samsung_cdcr_agent_alert.R;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Set;

public class AlertPeopleFragment extends Fragment {
  private static final String ARG_SECTION_NUMBER = "section_number";
  FirebaseFirestore db = FirebaseFirestore.getInstance();
  MainUser mnUser = MainUser.getInstance();
  private RecyclerView mRecyclerView;
  private AlertPeopleRecyclerAdapter mRecyclerAdapter;
  private RecyclerView.LayoutManager mLayoutManager;
  public DocumentReference currentAlertReference;
  public String alertID;
  View rootView;
  public Set<String> members;

  public ArrayList<Agent> myMembersList = new ArrayList<>();

  public AlertPeopleFragment() {
  }

  public static AlertPeopleFragment newInstance(int sectionNumber, Bundle b) {
    AlertPeopleFragment fragment = new AlertPeopleFragment();
    Bundle args = new Bundle();
    args.putInt(ARG_SECTION_NUMBER, sectionNumber);
    fragment.setArguments(b);
    return fragment;
  }

  public void getMembersOfAlert(long id) {
    alertID = Long.toString(id);
    db.collection("users").whereGreaterThanOrEqualTo("alerts."+alertID, 0)
            .addSnapshotListener(new EventListener<QuerySnapshot>() {
              @Override
              public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                  ObservableArrayList<Agent> people = new ObservableArrayList<>();
                people.addAll(queryDocumentSnapshots.toObjects(Agent.class));
                myMembersList = people;
                mRecyclerAdapter.notifyData(myMembersList);
              }
            });
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_alert_people, container, false);
    mRecyclerView = (RecyclerView) rootView.findViewById(R.id.members_list);
    mRecyclerAdapter = new AlertPeopleRecyclerAdapter(myMembersList);
    final LinearLayoutManager layoutManager = new LinearLayoutManager(this.getContext());
    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
    mRecyclerView.setLayoutManager(layoutManager);
    mRecyclerView.setAdapter(mRecyclerAdapter);
    /*mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
        if (recyclerView.canScrollVertically(1) && !recyclerView.canScrollVertically(-1)) {
          TabLayout view = (TabLayout) getActivity().findViewById(R.id.tabs);
          view.setVisibility(View.VISIBLE);
        }
        else if (recyclerView.canScrollVertically(1) || recyclerView.canScrollVertically(-1)) {
          TabLayout view = (TabLayout) getActivity().findViewById(R.id.tabs);
          view.setVisibility(View.GONE);
        }

      }

      @Override
      public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
      }
    });*/
    //Get Details from Firebase server and set view
    Bundle bundle = getArguments();
    if (bundle != null) {
      if (bundle.containsKey("cardListID")) {
        getMembersOfAlert(bundle.getLong("cardListID"));
      }
    }
    return rootView;
  }
}