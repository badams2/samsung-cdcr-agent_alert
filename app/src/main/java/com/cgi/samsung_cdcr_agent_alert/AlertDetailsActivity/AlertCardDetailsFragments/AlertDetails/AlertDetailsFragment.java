package com.cgi.samsung_cdcr_agent_alert.AlertDetailsActivity.AlertCardDetailsFragments.AlertDetails;

import android.content.Intent;
import android.databinding.Observable;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cgi.samsung_cdcr_agent_alert.Core.Utils.Maps.AlertMapUtils;
import com.cgi.samsung_cdcr_agent_alert.Core.Models.AlertData;
import com.cgi.samsung_cdcr_agent_alert.AlertLocationService.LocationService;
import com.cgi.samsung_cdcr_agent_alert.Core.MainUser;
import com.cgi.samsung_cdcr_agent_alert.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.maps.android.PolyUtil;
import com.google.maps.android.ui.IconGenerator;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.TravelMode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.annotation.Nullable;

public class AlertDetailsFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    AlertData currentCardDetails;
    TextView alertDetailsTextView;
    TextView locationDetailsTextView;
    TextView distanceDetailsTextView;
    TextView additionalInformation;
    private static final int overview = 0;
    FloatingActionButton floatingDirectionsButton;
    List<Address> addressesOrigin = new ArrayList<Address>();
    List<Address> addressesDestination = new ArrayList<Address>();
    MainUser mnUser = MainUser.getInstance();
    Observable.OnPropertyChangedCallback callback;
    int alertIndex;
    LocationService locSer;
    Location loc;
    Geocoder geocoder;
    View rootView;
    MapView mMapView;
    private GoogleMap googleMap;
    FirebaseFirestore db = FirebaseFirestore.getInstance();

    public AlertDetailsFragment() {
    }

    public static AlertDetailsFragment newInstance(int sectionNumber, Bundle b) {
        AlertDetailsFragment fragment = new AlertDetailsFragment();
        b.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onPause()
    {
        super.onPause();

    }
    private void setupGoogleMapScreenSettings(GoogleMap mMap) {
        mMap.setBuildingsEnabled(true);
        mMap.setIndoorEnabled(true);
        mMap.setTrafficEnabled(true);
        UiSettings mUiSettings = mMap.getUiSettings();
        mUiSettings.setMapToolbarEnabled(false);
        mUiSettings.setMyLocationButtonEnabled(false);
    }

    private void addMarkersToMap(DirectionsResult results, GoogleMap mMap) {
        IconGenerator fromIcon = new IconGenerator(getContext());
        fromIcon.setBackground(getResources().getDrawable(R.drawable.destination_ic));
        mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(fromIcon.makeIcon())).position(new LatLng(results.routes[overview].legs[overview].startLocation.lat, results.routes[overview].legs[overview].startLocation.lng)).title(results.routes[overview].legs[overview].startAddress));
        mMap.addMarker(new MarkerOptions().position(new LatLng(results.routes[overview].legs[overview].endLocation.lat, results.routes[overview].legs[overview].endLocation.lng)).title(results.routes[overview].legs[overview].endAddress).snippet(getEndLocationTitle(results)));
    }

    private void positionCamera(DirectionsRoute route, GoogleMap mMap) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        LatLng alertLatLngStart = new LatLng(route.legs[overview].startLocation.lat, route.legs[overview].startLocation.lng);
        LatLng alertLatLngEnd = new LatLng(route.legs[overview].endLocation.lat, route.legs[overview].endLocation.lng);
        builder.include(alertLatLngStart);
        builder.include(alertLatLngEnd);
        LatLngBounds bounds = builder.build();
        int padding = 15; // offset from edges of the map in pixels
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding));
    }

    private void addPolyline(DirectionsResult results, GoogleMap mMap) {
        List<LatLng> decodedPath = PolyUtil.decode(results.routes[overview].overviewPolyline.getEncodedPath());
        mMap.addPolyline(new PolylineOptions().addAll(decodedPath).width(10).color(Color.BLACK)
                .geodesic(true));
    }

    private String getEndLocationTitle(DirectionsResult results) {
        return "Time :" + results.routes[overview].legs[overview].duration.humanReadable + " Distance :" + results.routes[overview].legs[overview].distance.humanReadable;
    }

    public void getCurrentCardDetailsFromFirebase(long id) {
        db.collection("alerts").whereEqualTo("alertId", id).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w("CardListener", "Listen failed.", e);
                    return;
                }

                currentCardDetails = queryDocumentSnapshots.getDocuments().get(0).toObject(AlertData.class);
                alertDetailsTextView.setText(currentCardDetails.getName());
                locationDetailsTextView.setText(currentCardDetails.getFriendlyLocation());
                additionalInformation.setText(currentCardDetails.getAdditionalInfo());
                setUpMapAsync();

            }
        });

    }

    public void setUpMapAsync() {
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                geocoder = new Geocoder(getActivity().getApplicationContext(), Locale.getDefault());
                loc = new Location("system");
                loc.setLatitude(mnUser.myAgentData.get().location.getLatitude());
                loc.setLongitude(mnUser.myAgentData.get().location.getLongitude());
                if (loc != null) {
                    try {
                        addressesOrigin = geocoder.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
                    } catch (IOException e) {
                    }
                    // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    String addressOrigin = addressesOrigin.get(0).getAddressLine(0);
                    // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    googleMap = mMap;
                    try {
                        googleMap.setMyLocationEnabled(true);
                    } catch (SecurityException se) {

                    }
                    setupGoogleMapScreenSettings(googleMap);
                    DirectionsResult results = AlertMapUtils.getDirectionsDetails(addressOrigin, currentCardDetails.getFriendlyLocation(), TravelMode.DRIVING);
                    if (results != null) {
                        addPolyline(results, googleMap);
                        addMarkersToMap(results, googleMap);
                        positionCamera(results.routes[overview], googleMap);
                        String[] temp = AlertMapUtils.calculateDistanceAndDurations(results);
                        currentCardDetails.setAlertDistance(temp[0] + " Miles" + "  " + temp[1]);
                        distanceDetailsTextView.setText(currentCardDetails.getAlertDistance());

                    }
                    mMapView.onResume();
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_alert_details, container, false);
        alertDetailsTextView = (TextView) rootView.findViewById(R.id.alertDetailsCardContent);
        locationDetailsTextView = (TextView) rootView.findViewById(R.id.locationDetailsCardContent);
        distanceDetailsTextView = (TextView) rootView.findViewById(R.id.distanceDetailsCardContent);
        floatingDirectionsButton = (FloatingActionButton) rootView.findViewById(R.id.fabDirections);
        additionalInformation = (TextView) rootView.findViewById(R.id.additionalDetails);
        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        // needed to get the map to display immediately
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey("cardListID")) {
                getCurrentCardDetailsFromFirebase(bundle.getLong("cardListID"));
            }
        }

        floatingDirectionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentCardDetails != null) {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + currentCardDetails.getFriendlyLocation());
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                }
            }
        });
        return rootView;
    }

}