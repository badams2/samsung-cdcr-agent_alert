package com.cgi.samsung_cdcr_agent_alert.AlertDetailsActivity.AlertCardDetailsFragments.AlertPeople;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.cgi.samsung_cdcr_agent_alert.Core.Models.Agent;
import com.cgi.samsung_cdcr_agent_alert.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AlertPeopleRecyclerAdapter extends RecyclerView.Adapter<AlertPeopleRecyclerAdapter.RecyclerItemViewHolder> {
    private ArrayList<Agent> myList;

    public AlertPeopleRecyclerAdapter(ArrayList<Agent> myList) {
        this.myList = myList;
    }
    public RecyclerItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.agent_info_item, parent, false);
        RecyclerItemViewHolder holder = new RecyclerItemViewHolder(view);
        return holder;
    }
    @Override
    public int getItemCount() {
        return(null != myList?myList.size():0);
    }
    public String amountOfAgentsWithTitle(String title)
    {
        int count = 0;
        for(int i = 0; i < myList.size(); i++)
        {
            if(myList.get(i).getTitle().equals(title))
                count++;
        }
        return Integer.toString(count);
    }
    @Override
    public void onBindViewHolder(RecyclerItemViewHolder holder, final int position) {
        Log.d("onBindViewHoler ", myList.size() + "");
        String titleCount = amountOfAgentsWithTitle(myList.get(position).getTitle());
        holder.headerTitle.setText(myList.get(position).getTitle());
        holder.headerAmount.setText(" " + "(" + titleCount  + ")");

        if(isFirstInGroup(position))
        {
            holder.headerLayout.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.headerLayout.setVisibility(View.GONE);
        }
        holder.agentName.setText(myList.get(position).getFirstName() + " " + myList.get(position).getLastName());
        holder.agentID.setText("ID: " + myList.get(position).getAgentId());
        holder.agentPhone.setText(myList.get(position).getPhone());
        Glide.with(holder.itemView.getContext()).load(myList.get(position).getAvatar()).into(holder.profileImage);
    }
    public boolean isFirstInGroup(int position) {
        if (position == 0) {
            return true;
        }
        if (!(myList.get(position).getTitle().equals(myList.get(position - 1).getTitle()))) {
            return true;
        }
        return false;
    }

    public void notifyData(ArrayList<Agent> myList) {
        this.myList = myList;
        notifyDataSetChanged();
    }
    public class RecyclerItemViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout headerLayout;
        private TextView headerTitle;
        private TextView headerAmount;
        private CircleImageView profileImage;
        private TextView agentName;
        private TextView agentPhone;
        private TextView agentID;
        private ImageView agentStatus;
        private ImageButton callButtonView;
        public RecyclerItemViewHolder(final View parent) {
            super(parent);
            headerLayout = (LinearLayout) parent.findViewById(R.id.header_id);
            headerTitle = (TextView) parent.findViewById(R.id.header_title);
            headerAmount = (TextView) parent.findViewById(R.id.header_amount);
            profileImage = (CircleImageView) parent.findViewById(R.id.profile_image);
            agentName = (TextView) parent.findViewById(R.id.agent_name);
            agentID = (TextView) parent.findViewById(R.id.agent_id);
            agentPhone = (TextView) parent.findViewById(R.id.agent_phone);
            agentStatus = (ImageView) parent.findViewById(R.id.online_status);
            callButtonView = (ImageButton) parent.findViewById(R.id.callButton);
            callButtonView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try
                    {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + myList.get(getPosition()).getPhone()));
                        itemView.getContext().startActivity(intent);
                    }
                    catch (SecurityException e)
                    {

                    }
                }
            });
        }
    }
}