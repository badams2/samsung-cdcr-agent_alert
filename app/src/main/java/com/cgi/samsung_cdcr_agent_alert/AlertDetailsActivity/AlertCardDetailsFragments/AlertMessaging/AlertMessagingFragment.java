package com.cgi.samsung_cdcr_agent_alert.AlertDetailsActivity.AlertCardDetailsFragments.AlertMessaging;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.cgi.samsung_cdcr_agent_alert.AlertCards.RemoveClickListner;
import com.cgi.samsung_cdcr_agent_alert.Core.MainUser;
import com.cgi.samsung_cdcr_agent_alert.Core.Models.Message;
import com.cgi.samsung_cdcr_agent_alert.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.WriteBatch;

import java.util.ArrayList;
import java.util.Date;

public class AlertMessagingFragment extends Fragment implements RemoveClickListner {


  private RecyclerView mRecyclerView;
  private AlertMessagingRecyclerAdapter mRecyclerAdapter;
  FloatingActionButton scrollDownButton;
  private RecyclerView.LayoutManager mLayoutManager;
  TabLayout tabView;
  EditText messageText;
  ImageButton sendButton;
  public ArrayList<Message> myMessagesList = new ArrayList<>();
  public DocumentReference currentAlert;
  View rootView;
  FirebaseFirestore db = FirebaseFirestore.getInstance();
  private static final String ARG_SECTION_NUMBER = "section_number";

  public AlertMessagingFragment() {
    // Required empty public constructor
  }

  public static AlertMessagingFragment newInstance(int sectionNumber, Bundle b) {
    AlertMessagingFragment fragment = new AlertMessagingFragment();
    b.putInt(ARG_SECTION_NUMBER, sectionNumber);
    fragment.setArguments(b);
    return fragment;
  }

  public void getCurrentAlertReference(long id) {
    db.collection("alerts")
            .whereEqualTo("alertId", id)
            .addSnapshotListener(new EventListener<QuerySnapshot>() {
              @Override
              public void onEvent(@Nullable QuerySnapshot value,
                                  @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                  Log.w("MessageListener", "Listen failed.", e);
                  return;
                }
                DocumentReference alertDocument = value.getDocuments().get(0).getReference();
                currentAlert = alertDocument;
                messageListener();
              }
            });
  }

  public void messageListener() {
    currentAlert.collection("Messages").addSnapshotListener(new EventListener<QuerySnapshot>() {
      @Override
      public void onEvent(QuerySnapshot value, FirebaseFirestoreException e) {
        if (e != null) {
          Log.w("MessagesListener", "Listen failed.", e);
          return;
        }
        final ArrayList<Message> messages = new ArrayList<>();
        for (QueryDocumentSnapshot doc : value) {
          Message msg = doc.toObject(Message.class);
          msg.setDocRef(doc.getReference());
          messages.add(msg);
        }
        myMessagesList = messages;
        mRecyclerAdapter.notifyData(myMessagesList);
      }
    });
  }
  /*public void setUpTypingListener()
  {
      currentAlert.collection("Members")
              .document(MainUser.getInstance().firestoreDBID)
              .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                @Override
                public void onEvent(DocumentSnapshot documentSnapshot, FirebaseFirestoreException e) {
                  if(e != null)
                  {
                    Log.d("Members Listener", "FAILED: " + e);
                    return;
                  }
                  final ArrayList<Message> messages = new ArrayList<>();
                  for (QueryDocumentSnapshot doc : value) {
                    messages.add(doc.toObject(Message.class));
                  }
                }
              });
  }*/

  public void sendTextWatcherResultToFirebase(boolean isTyping) {
    if (isTyping)
      currentAlert.collection("Members")
              .document(MainUser.getInstance().firestoreDBID)
              .update("isTyping", true);
    else {
      currentAlert.collection("Members")
              .document(MainUser.getInstance().firestoreDBID)
              .update("isTyping", false);
    }
  }

  public void sendMessage() {
    String msg = messageText.getText().toString();
    if (!msg.equals("")) {
      final Message myMsg = new Message();
      myMsg.setMsg(msg);
      myMsg.setFrom(MainUser.getInstance().myAgentData.get());
      myMsg.setMsgSentTime(new Date());
      myMsg.getSeenBy().put(MainUser.getInstance().firestoreDBID, true);
      myMessagesList.add(myMsg);
      mRecyclerAdapter.notifyData(myMessagesList);
      currentAlert.collection("Messages")
              .add(myMsg)
              .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                @Override
                public void onSuccess(DocumentReference documentReference) {
                  Log.d("Message Sender", documentReference.getId());
                  mRecyclerAdapter.notifyData(myMessagesList);
                  mRecyclerView.scrollToPosition(myMessagesList.size() - 1);
                }
              })
              .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                  Log.w("Message Sender", "Error adding document", e);
                }
              });
    }
  }
  @Override
  public void setUserVisibleHint(boolean visible) {
    super.setUserVisibleHint(visible);
    if (visible) {
      Log.d("Message Fragment", "Looking at it now");
      WriteBatch batch = db.batch();
      for(Message m : this.myMessagesList)
      {
          Log.d("TEST","TEST");
        batch.update(m.getDocRef(),"seenBy."+ MainUser.getInstance().firestoreDBID, true);
      }
      batch.commit().addOnCompleteListener(new OnCompleteListener<Void>() {
        @Override
        public void onComplete(@NonNull Task<Void> task) {
          Log.d("Messages Seen By Batch", "COMPLETE");
        }
      });
    }
  }
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_alert_messaging, container, false);
    mRecyclerView = (RecyclerView) rootView.findViewById(R.id.messageRecyclerView);
    mRecyclerAdapter = new AlertMessagingRecyclerAdapter(myMessagesList, this);
    scrollDownButton = (FloatingActionButton) rootView.findViewById(R.id.messagesScrollDown);
    messageText = (EditText) rootView.findViewById(R.id.messageEdit);
    messageText.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        //sendTextWatcherResultToFirebase(false);
      }

      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        //sendTextWatcherResultToFirebase(true);
      }

      @Override
      public void afterTextChanged(Editable editable) {
        //sendTextWatcherResultToFirebase(false);
        if(editable.toString().trim().length()>0){
          sendButton.setBackground(getResources().getDrawable(R.drawable.active_send_icon));
        }else{
          sendButton.setBackground(getResources().getDrawable(R.drawable.inactive_send_icon));
        }

      }
    });
    sendButton = (ImageButton) rootView.findViewById(R.id.chatSendButton);
    sendButton.setBackground(getResources().getDrawable(R.drawable.inactive_send_icon));
    sendButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        sendMessage();
        messageText.setText("");
      }
    });
    final LinearLayoutManager layoutManager = new LinearLayoutManager(this.getContext());
    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
    mRecyclerView.setLayoutManager(layoutManager);
    mRecyclerView.setAdapter(mRecyclerAdapter);
    mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
        if(recyclerView.canScrollVertically(-1) && recyclerView.canScrollVertically(1))
        {
          scrollDownButton.setVisibility(View.VISIBLE);
        }
        else if (recyclerView.canScrollVertically(-1) & !recyclerView.canScrollVertically(1))
        {
          scrollDownButton.setVisibility(View.GONE);
        }

      }

      @Override
      public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
      }
    });
    mRecyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
      @Override
      public void onLayoutChange(View v,
                                 int left, int top, int right, int bottom,
                                 int oldLeft, int oldTop, int oldRight, int oldBottom) {
        if (bottom < oldBottom) {
          mRecyclerView.postDelayed(new Runnable() {
            @Override
            public void run() {

              mRecyclerView.scrollToPosition(
                      mRecyclerView.getAdapter().getItemCount() - 1);
            }
          }, 100);
        }
      }
    });
    scrollDownButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        mRecyclerView.scrollToPosition(
                mRecyclerView.getAdapter().getItemCount() - 1);
        scrollDownButton.setVisibility(View.GONE);
      }
    });
    Bundle bundle = getArguments();
    if (bundle != null) {
      if (bundle.containsKey("cardListID")) {
        getCurrentAlertReference(bundle.getLong("cardListID"));
      }
    }
    return rootView;
  }

  @Override
  public void OnRemoveClick(int index) {
    myMessagesList.remove(index);
    mRecyclerAdapter.notifyData(myMessagesList);
  }

}