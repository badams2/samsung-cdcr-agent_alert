package com.cgi.samsung_cdcr_agent_alert;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class MainAgentAlertActivityTestCase {

    @Rule
    public ActivityTestRule<MainAgentAlertActivity> mMainAgentAlertActivityActivityTestRule
            = new ActivityTestRule<>(MainAgentAlertActivity.class);

    @Test
    public void clickAlertCard_Opens_AlertCardDetailsActivity() throws Exception
    {

    }
}
